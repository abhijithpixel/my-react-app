import React , {useState  , useRef} from 'react'

const NewItem = (props) => {

  const [newListItemId , setNewListItemId] = useState("")
  const [newListItemName , setNewListItemName] = useState("")
  const [newListItemPrice , setNewListItemPrice] = useState("")
  const [newListItemDate , setNewListItemDate] = useState("")

 const newItem = {
   "id": newListItemId,
   "name": newListItemName,
   "price": newListItemPrice,
   "date": newListItemDate
 }
const itemName = useRef();
  const updateItemId =(event)=>{
    setNewListItemId(event.target.value);
  } 
  const updateItemName =(event)=>{
    setNewListItemName(event.target.value);
  } 
  const updateItemPrice =(event)=>{
    setNewListItemPrice(event.target.value);
  } 
  const updateItemDate =(event)=>{
    setNewListItemDate(event.target.value);
  } 
  // console.log()


const submitHandler=(event)=>{
  event.preventDefault();
  console.log(itemName.current.value);
  props.addNewItem(newItem);
  setNewListItemId("");
    setNewListItemPrice("");
    setNewListItemName("");
    setNewListItemDate("");

}

  return (
    <form onSubmit={submitHandler}>
      <fieldset>
        <label >id:</label>
        <input type="text" value={newListItemId} onChange={updateItemId} ref={itemName}/>
      </fieldset>
      <fieldset>
        <label >name:</label>
        <input type="text" value={newListItemName} onChange={updateItemName} />
      </fieldset>
      <fieldset>
        <label >price:</label>
        <input type="text" value={newListItemPrice} onChange={updateItemPrice}/>
      </fieldset>
      <fieldset>
        <label >date:</label>
        <input type="date" value={newListItemDate} onChange={updateItemDate}/>
      </fieldset>
      <input type="submit" value="ADD ITEM" />
    </form>
  )
}

export default NewItem;
