import React from 'react'
import CurrencyFormat from 'react-currency-format';
import { getCartTotal } from '../reducer';
import { UseStateValue } from '../StateProvider';

const SubTotal = () => {
    const [{basket}] = UseStateValue();
    return (

        <div className="subtotal__entries">
            <CurrencyFormat
            renderText={(value) => (
                <>
            <span>Subtotal ({basket.length}items):<p>{(value)}</p> </span>
            <div className="gift_card-block">
                {/* <input type="checkbox" id="gift-card"/> */}
                {/* <label htmlFor="gift-card">This order contains a gift</label> */}
            </div>

                </>
            )}
            decimalScale={2}
            value={getCartTotal(basket)}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"₹"}
            />
            <button className="btn btn-yellow">Continue to checkout</button>
            
        </div>
    )
}

export default SubTotal;
