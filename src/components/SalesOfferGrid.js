import React from 'react'

import Salescard from './Salescard';

const SalesOfferGrid = (props) => {
    return (
        <div className="sales-offers-card">
        <h4>{props.gridhead}</h4>
        <div className="sales-offers_grid ">
            <div className="row">
                <div className="col-md-6">
                     <Salescard tagline={props.cardhead1} img={props.image1}/>
                </div>
                <div className="col-md-6">
                     <Salescard tagline={props.cardhead2} img={props.image2}/>
                </div>
            </div>
            <div className="row">

                <div className="col-md-6">
                    <Salescard tagline={props.cardhead3} img={props.image3}/>
                </div>
                <div className="col-md-6">
                    <Salescard tagline={props.cardhead4} img={props.image4}/>
                </div>
            </div>
        </div>
        <button>{props.gridbtn}</button>
    </div>
    )
}

export default SalesOfferGrid;
