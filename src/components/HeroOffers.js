import React from 'react'

import SalesOfferGrid from './SalesOfferGrid';
import cardimage1 from "../uploads/image.jpg"
import cardimage2 from "../uploads/image2.jpg"
import cardimage3 from "../uploads/image3.jpg"
import cardimage4 from "../uploads/image4.jpg"
import cardimage5 from "../uploads/image5.jpg"
import cardimage6 from "../uploads/image6.jpg"
import cardimage7 from "../uploads/image7.jpg"
import cardimage8 from "../uploads/image8.jpg"
import cardimage9 from "../uploads/image9.jpg"
import cardimage10 from "../uploads/image10.jpg"
import cardimage11 from "../uploads/image11.jpg"
import cardimage12 from "../uploads/image12.jpg"
import cardimage13 from "../uploads/image13.jpg"
import cardimage14 from "../uploads/image14.jpg"
import cardimage15 from "../uploads/image15.jpg"
import cardimage16 from "../uploads/image16.jpg"
import cardimage17 from "../uploads/image17.jpg"
import cardimage18 from "../uploads/image18.jpg"
import cardimage19 from "../uploads/image19.jpg"
import cardimage20 from "../uploads/image20.jpg"
import cardimage21 from "../uploads/image21.jpg"
import cardimage22 from "../uploads/image22.jpg"
import cardimage23 from "../uploads/image23.jpg"
import cardimage24 from "../uploads/image24.jpg"
import cardimage25 from "../uploads/image25.jpg"
import cardimage26 from "../uploads/image26.jpg"
import cardimage27 from "../uploads/image27.jpg"
import cardimage28 from "../uploads/image28.jpg"

import Salesoffergridempty from './Salesoffergridempty';


const HeroOffers = () => {
    return (
        <section className="hero_sales_n_offers">
            <div className="container">
                <div className="hero_sales_n_offers-wrap">
                    <div className="row">
                        <div className="col-md-3">
                            <SalesOfferGrid gridhead="Save up to ₹9,000 | Mobiles to suit your budget" cardhead1="Budget: Flat 10% off with SBI credit cards" cardhead2="Mid-range: Up to 6 months No Cost EMI" cardhead3="Premium: Up to ₹2,000 off with coupons" cardhead4="Ultra Premium: Save up to ₹9,000" image1={cardimage1} image2={cardimage2} image3={cardimage3} image4={cardimage4} gridbtn="See all deals"/>
                        </div>
                        <div className="col-md-3">
                        <SalesOfferGrid gridhead="Up to 70% off | Home & kitchen" cardhead1="Kitchen & home appliances" cardhead2="Home & decor" cardhead3="Cookware & dining" cardhead4="Sports & fitness" image1={cardimage5} image2={cardimage6} image3={cardimage7} image4={cardimage8} gridbtn="See all deals"/>
                        </div>
                        <div className="col-md-3">
                        <SalesOfferGrid gridhead="Up to 60% off | Daily essentials" cardhead1="Health & nutrition" cardhead2="Snacks & beverages" cardhead3="Baby products" cardhead4="Personal care essentials" image1={cardimage9} image2={cardimage10} image3={cardimage11} image4={cardimage12} gridbtn="See all deals"/>
                        </div>
                        <div className="col-md-3">
                        <Salesoffergridempty gridhead="Check out latest collection of books"/>
                        </div>
                        <div className="col-md-3">
                        <SalesOfferGrid gridhead="Up to 55% off | Best-selling TVs & appliances" cardhead1="Latest launches in TVs | Up to 45% off" cardhead2="Starting ₹6,999/- | Washing machines" cardhead3="Starting ₹8,099 | All TVs" cardhead4="Starting ₹6,790 | Refrigerators" image1={cardimage13} image2={cardimage14} image3={cardimage15} image4={cardimage16} gridbtn="See all deals"/>
                        </div>
                        <div className="col-md-3">
                        <SalesOfferGrid gridhead="Up to 80% off | Popular categories in fashion" cardhead1="Men’s innerwear" cardhead2="Women’s sarees" cardhead3="Men’s footwear" cardhead4="Watches" image1={cardimage17} image2={cardimage18} image3={cardimage19} image4={cardimage20} gridbtn="See all deals"/>
                        </div>
                        <div className="col-md-3">
                        <SalesOfferGrid gridhead="Professional tools, testing & more" cardhead1="Professional tools" cardhead2="Measuring instruments" cardhead3="Cleaning supplies" cardhead4="Medical supplies" image1={cardimage21} image2={cardimage22} image3={cardimage23} image4={cardimage24} gridbtn="See all deals"/>
                        </div>
                        <div className="col-md-3">
                        <SalesOfferGrid gridhead="Automotive essentials | Up to 60% off" cardhead1="Cleaning accessories" cardhead2="Tyre & rim care" cardhead3="Helmets" cardhead4="Vacuum cleaner" image1={cardimage25} image2={cardimage26} image3={cardimage27} image4={cardimage28} gridbtn="See all deals"/>
                        </div>
                        {/* <div className="col-md-3">
                        <SalesOfferGrid gridhead="Best selling Books | Up to 60% off" cardhead1="Big Shot Diary of a Wimpy Kid Book 16" cardhead2="Dune (Dune Chronicles, Book 1)" cardhead3="Best Wishes, Warmest Regards: The Story of…" cardhead4="Fizban's Treasury of Dragons (Dungeon &…" image1="https://images-na.ssl-images-amazon.com/images/I/81z6xJd6mrS._AC_UL200_SR200,200_.jpg" image2="https://images-na.ssl-images-amazon.com/images/I/910N36lR%2BkL._AC_UL200_SR200,200_.jpg" image3="https://images-na.ssl-images-amazon.com/images/I/91s5VXjRm9S._AC_UL200_SR200,200_.jpg" image4="https://images-na.ssl-images-amazon.com/images/I/81vkPphfchS._AC_UL200_SR200,200_.jpg" gridbtn="See all deals"/>
                        </div> */}
                    </div>
                </div>
            </div>

            
        </section>
    )
}

export default HeroOffers;
