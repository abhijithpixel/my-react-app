import React from 'react'
import { Link } from 'react-router-dom';
const LogOutHeader = () => {
  return (
    <header  className="">
            <div className="container">
                <nav>
                    <div className="left_nav">

                        <Link to="/" className="logo">
                            <span className="logo-big">AJ</span> Store <span>Shopping Made Easier</span>
                        </Link>

                    </div>
            
                </nav>
            </div>
        </header>
  )
}

export default LogOutHeader;
