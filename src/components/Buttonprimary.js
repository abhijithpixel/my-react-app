import React from 'react'
import { Link } from 'react-router-dom';
const Buttonprimary = (props) => {
    return (
        <Link to="/login" className="btn btn-yellow">
            {props.btncontent}
        </Link>
    )
}

export default Buttonprimary;
