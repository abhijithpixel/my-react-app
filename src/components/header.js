import React from 'react'
import { Link } from 'react-router-dom';
import { UseStateValue } from '../StateProvider';
import { useState } from 'react';
import cartImg from '../uploads/cart.svg';
const Header = (props) => {
    const [{basket}] = UseStateValue();
     const [isNavOpen, setisNavOpen] = useState(false);

        const  toggleNav = () => {
            isNavOpen ? setisNavOpen(false) : setisNavOpen(true);
            bodyNoscroll();
        }
        const closeNav = () =>{
            setisNavOpen(false);
            document.body.classList.remove('overflow_hidden');
        } 
        const bodyNoscroll = () => {
            document.body.classList.toggle('overflow_hidden');
        }
        const logOutUser=()=>{
        localStorage.setItem('UserLoggedIn', '0');
        // window.location.reload(true);
        props.logegedIn(0);
        }
    return (
        <header  className={isNavOpen ? " mobile_nav_open" : "mobile_nav_closed"}>
            <div className="container">
                <nav>
                    <div className="left_nav">

                        <Link to="/" className="logo">
                            <span className="logo-big">AJ</span> Store <span>Shopping Made Easier</span>
                        </Link>
                        <ul className="mobile_hamburger_menu" onClick={toggleNav}>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                    <ul className="nav-links-wrap">
                        <li onClick={closeNav}><Link to="/products">Products</Link></li>
                        <li onClick={closeNav} ><Link to="/weather">Weather</Link></li>
                        <li onClick={closeNav} ><Link to="/book-list">Books</Link></li>
                        <li onClick={closeNav}><Link to="/contact" className="">Contact Me</Link></li>
                        <li  onClick={closeNav}><Link to="/checkout">Cart<sup>{basket.length}</sup></Link></li>
                        <li onClick={closeNav}><button className="banner-btn" onClick={logOutUser}>Log out</button></li>

                    </ul>
                </nav>
            </div>
        </header>
    )
}

export default Header;
