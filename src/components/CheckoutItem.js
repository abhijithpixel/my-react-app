import React , {useState} from 'react'
import { UseStateValue } from '../StateProvider';

const CheckoutItem = (props) => {
    const [{basket},dispatch] = UseStateValue();
    const [pdtQty , setpdtQty] = useState(1);
    const increaseqty = () => {
        setpdtQty(
            prevCount => prevCount + 1
        )
    }
    const descreaseqty = () => {
        setpdtQty(
            prevCount => prevCount - 1
        )
    }
    // console.log(props.image ,props.id ,  props.title ,props.rating , props.price);
 const removeFromBasket = () => {
    //  remove item from cart
    dispatch({
        type:"REMOVE_FROM_BASKET",
        id:props.id,
    });
 }

    return (
        <div className="cart_item row">
            <div className="item_img col-md-3">
            <img src={props.image} alt="" />
            </div>
            <div className="item_info col-md-9">
                <p className="pdt_id">{props.id}</p>
                    <p className="pdt_name">{props.title}</p>
                    {/* <span className="pdt-qty">qty:<button onClick={descreaseqty}>-</button>{pdtQty}<button onClick={increaseqty}>+</button></span>  */}
                    <div className="pdt_rating">
                    {
                    Array(props.rating)
                        .fill()
                        .map(() =>(
                            <p>⭐</p>
                        ))} 
                        <span>{props.rating}/5</span>
                </div>
                <p className="pdt_price">₹: <span>{props.price}</span> </p>
                <button className="btn btn-yellow" onClick={removeFromBasket}>Remove from basket</button>
            </div>
        </div>
    )
}
// CheckoutItem.defaultProps = {
//     pdtQty: 1,
  
//   }
export default CheckoutItem;
