import React from 'react'
import Buttonprimary from './Buttonprimary'
import { Link } from 'react-router-dom'
const Salesoffergridempty = (props) => {
    return (
        <div className="sales-offers-card">
        <h4>{props.gridhead}</h4>
      {/* <Buttonprimary  btncontent="Books"/> */}
      <Link to="./book-list" className="btn btn-yellow">Book list</Link>
        
        </div>
    )
}

export default Salesoffergridempty;
