import React from 'react'
import { UseStateValue } from '../StateProvider';
// import { Link } from 'react-router-dom';
// import ProductInfo from '../ProductInfo';
const Product = (props) => {
    const [{basket} , dispatch] = UseStateValue();

    const addToCart = () => {
        dispatch({
            type:'ADD_TO_BASKET',
            item:{
                id:props.pdtId,
                title:props.pdtName,
                image:props.pdtImg,
                price:props.pdtPrice,
                rating:props.pdtRating,
                qty:props.pdtQty,
            },
        });
    };


    return (
        <div  className="product_item" >
            <div className="pdt-image">
                <img src={props.pdtImg} alt="" />
            </div>
            <p className="pdt_name">{props.pdtName}
            </p>
            <div className="pdt_rating">
                {
                Array(props.pdtRating)
                    .fill()
                    .map(() =>(
                        <p>⭐</p>
                    ))} 
                    <span>{props.pdtRating}/5</span>
            </div>
            <p className="pdt_id">{props.pdtId}</p>
            <p className="pdt_price">₹: <span>{props.pdtPrice}</span> </p>
  
            
                        <a href={props.buyLink} className="btn btn-yellow" target="_blank" /*onClick={addToCart}*/>Buy From Amazon</a>
                        {/* <button  className="btn btn-yellow"  onClick={addToCart}>Add to cart</button> */}
        
        </div>
    )
}

Product.defaultProps = {
  pdtQty:1,
  }

export default Product;
