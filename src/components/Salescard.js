import React from 'react'
import { Link } from 'react-router-dom';
const Salescard = (props) => {
    return (
        <Link to="/products" className="sales_card">
            <img src={props.img} alt="" />
            <h6>{props.tagline}</h6>
        </Link>
    )
}

export default Salescard;
