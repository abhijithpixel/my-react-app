import React , {useState } from 'react'
import axios from 'axios'

const Weather = () => {

  const [userInput, setUserInput] = useState({
    place:"",
    temp:""
  })
  const submitHandler = (event)=>{
    event.preventDefault();
    console.log(userInput)
    const newWeather = {
      place : userInput.place,
      temp : userInput.temp
    }
    axios.post('http://localhost:4001/create', newWeather);
    setUserInput({place:"",temp:""})
  }
  
  const handleChange =(event)=>{
    const {name , value} =event.target;
    setUserInput(prevState =>{
      return{...prevState , [name]: value
      }
    })
}
const [weathers, setWeather] = useState({
  place:'',
  temp:''
})

const useEffect=()=>{
  fetch("/weathers").then(res =>{
    if(res.ok){
      return res.json()
    }
  }).then(jsonRes => setWeather(jsonRes));
}

  return (
    <>
      {/* <Header /> */}
    <div className="container weather-data">
      <form >
        <fieldset>
          <label htmlFor="place">Place:</label>
          <input type="text" id="place" name="place" value={userInput.place} onChange={handleChange}/>
        </fieldset>
        <fieldset>
          <label htmlFor="temp">Temperature:</label>
          <input type="text" id="temp" name="temp" value={userInput.temp} onChange={handleChange}/>
        </fieldset>
        <input type="submit" className="btn btn-success" onClick={submitHandler}/>
      </form>
      <ul className="weather_data">
        {
          // weathers.map((weather)=>{
          //   return <li>{weather.place}{weather.temp}</li>
            
          // })
        }
      </ul>
    </div>
    </>
  )
}

export default Weather;
