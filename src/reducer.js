import {viewitem} from './ViewItem'
export const initialState = {
    basket:[
// {
//     id:"74638598",
//     title:"pTron Bassbuds Ultima Active Noise Cancelling Bluetooth 5.0 Wireless Headphones with Immersive Sound & Deep Bass, HD Mic, Touch Control Earbuds, Voice Assistance & IPX4 Water-Resistant (Black)",
//     rating:4,
//     price:344,
//     image:"https://images-na.ssl-images-amazon.com/images/I/51cRp7B4hnS._SL1100_.jpg"
// },

    ],
    user:null,
};

export const getCartTotal = (basket) => 
    basket?.reduce((amount , item) => item.price  + amount, 0 );



function reducer(state , action)  {
    // console.log(action);
    switch(action.type){
        case 'ADD_TO_BASKET':
        //logic for adding item to basket
        return {
            ...state,
            basket:[...state.basket, action.item]
        };
        case 'REMOVE_FROM_BASKET':
            //logic for removing item from basket
            let newBasket = [...state.basket];
            const index = state.basket.findIndex((basketItem) => basketItem.id === action.id);
            // alert(basketItem.id);
            if(index >= 0){
                // item exist
                newBasket.splice(index , 1);
            }else{
     
            }
        return {
            ...state ,
            basket: newBasket
        };
        case 'SHOW_DETAIL':
            return{
                // alert(action)
                // ...store
            }
        case 'VIEW_ITEM':
            console.log(action.item.title);
            // console.log(action);
        

            return{
                ...state
            };
        default:
            return {...state};
    }
}

export default reducer;