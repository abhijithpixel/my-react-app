import React , {useState } from 'react'
// import axios from './axios';
import NewItem from './NewItem';



const ProductInfo = (props) => {

const [groceryList, setGroceryList] = useState([
    {
        "id":"123",
        "name":"milk",
        "price":"20",
        "date": "2021-11-18"
    },
    {
        "id":"143",
        "name":"egg",
        "price":"5",
        "date": "2020-11-18"
    },
    {
        "id":"178",
        "name":"rice",
        "price":"40",
        "date": "2019-11-18"
    },
]);


// const [people , setPeople] = useState([]);
const [selectedDate, setselectedDate] = useState("2020")
// useEffect(() => {
//  async function fetchData(){
//      const req = await axios.get('/people/users');
//      setPeople(req.data);
//  }
//  fetchData();
// }, [])
const addToList=(newItem)=>{
// console.log(newItem);
setGroceryList(prevState =>([ ...prevState, newItem]))
}



// const filteredList = groceryList.filter(filterItem => {
//     // console.log(selectedDate);
//     // return filterItem.date.getFullYear() == selectedDate;
    
//     // console.log(filterItem.date);

// })
const deleteItem=(event)=>{
console.log(event.currentTarget.parentElement.getAttribute('key'));
}
const changeSelectedDate = event=>{
    setselectedDate(event.target.value)
}
    return (
        <>
               <div className="info_slider">
                   <div className="container">
                       
               {/* <button onClick={()=> getJsonData()}>Get data</button> */}
               <NewItem addNewItem={addToList}/>
               <select name="date" id="date" value={selectedDate} onChange={changeSelectedDate}>
                   <option value="2022">2022</option>
                   <option value="2021">2021</option>
                   <option value="2020">2020</option>
                   <option value="2019">2019</option>
                   <option value="2018">2018</option>

               </select>
               <ul className="usercard">
                   {
                       groceryList.map((item)=>{
                           return <li key={item.id}> <p>{item.name}</p> <span>{item.date} </span> <p>{item.price}</p> <strong onClick={deleteItem}>DEL</strong> </li>
                       }
                       )}
               </ul>
                   </div>
               </div>
          
        </>
    )
// return React.createElement('div' , {} , React.createElement('h1',{}, 'hi '));
}

export default ProductInfo;
