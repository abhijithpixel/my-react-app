export const books =[

  {
      id: 1,
      rating: 4,
      price: 600,
      img: 'https://images-na.ssl-images-amazon.com/images/I/81vkPphfchS._AC_UL200_SR200,200_.jpg',
      title: 'Fizbans Treasury of Dragons (Dungeon)'
  },
  {
      id: 2,
      rating: 4,
      price: 768,
      img: 'https://images-na.ssl-images-amazon.com/images/I/81GXl0dweAL._AC_UL200_SR200,200_.jpg',
      title: 'Paint by Sticker Kids: Christmas: Create 1'
  },
  {
      id: 3,
      rating: 5,
      price: 293,
      img: 'https://images-na.ssl-images-amazon.com/images/I/81YHHiNPDNL._AC_UL200_SR200,200_.jpg',
      title: 'The Storyteller: Tales of Life and Music'
  },
  {
      id: 4,
      rating: 3,
      price: 496,
      img: 'https://images-na.ssl-images-amazon.com/images/I/71ki8f20R7S._AC_UL200_SR200,200_.jpg',
      title: 'The Judges List: A Novel'
  },
  {
      id: 5,
      rating: 1,
      price: 700,
      img: 'https://images-na.ssl-images-amazon.com/images/I/612V9dt-NGS._AC_UL200_SR200,200_.jpg',
      title: 'Burn After Writing (Pink)'
  },
  {
      id: 6,
      rating: 4,
      price: 445,
      img: 'https://images-na.ssl-images-amazon.com/images/I/81iAADNy2NL._AC_UL200_SR200,200_.jpg',
      title: 'Atomic Habits: An Easy & Proven Way to…'
  },
  {
      id: 7,
      rating: 2,
      price: 375,
      img: 'https://images-na.ssl-images-amazon.com/images/I/61NdJMwAThS._AC_UL200_SR200,200_.jpg',
      title: 'The Body Keeps the Score: Brain, Mind, and…'
  },
  {
      id: 8,
      rating: 2,
      price: 400,
      img: 'https://images-na.ssl-images-amazon.com/images/I/71xUvuJiqgL._AC_UL200_SR200,200_.jpg',
      title: 'It Ends with Us: A Novel'
  },
  {
      id: 9,
      rating: 3,
      price: 765,
      img: 'https://images-na.ssl-images-amazon.com/images/I/71hh%2BSn5WOS._AC_UL200_SR200,200_.jpg',
      title: 'Rigged: How the Media, Big Tech, and the…'
  },
  {
      id: 10,
      rating: 4,
      price: 488,
      img: 'https://images-na.ssl-images-amazon.com/images/I/91iAPfpZdJL._AC_UL200_SR200,200_.jpg',
      title: 'Renegades Born in the USA'
  },
  {
      id: 11,
      rating: 3,price: 333,
      img: 'https://images-na.ssl-images-amazon.com/images/I/81nzxODnaJL._AC_UL200_SR200,200_.jpg',
      title: 'If Animals Kissed Good Night'
  },
];