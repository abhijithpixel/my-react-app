import React from 'react'
import { UseStateValue } from './StateProvider';
import CheckoutItem from './components/CheckoutItem';
import SubTotal from './components/SubTotal';
const Checkout = () => {
    const [{basket}] = UseStateValue();
    return (
        <>
       <div className="container">
       <div className="checkout_basket-listing">
           <h2>Shopping Cart</h2>
                {basket?.length === 0 ? (
                    <h3>Your cart is empty</h3>
                ):(
                    <div className="listing__wrap row">
                        <div className="lists-item col-md-9">
                            {basket?.map((item) => (
                            <CheckoutItem id={item.id} title={item.title} image={item.image} rating={item.rating} price={item.price} qty={item.qty}/>
                            ))}
                        </div>
                        <div className="checkout-total col-md-3">
                <div >{basket.length > 0 && (
                    <SubTotal/>
                )}</div>
            </div>
            </div>
          
                )}
             
       </div>
       </div>
       
   </>
    )
}

export default Checkout;
