import React from 'react'
import { books } from './Store';
import { UseStateValue } from './StateProvider';
import { Link } from 'react-router-dom';
const BookList = () => {
    return (
        <section className="book-list">
            <div className="container" >
                    <h2>Amazon Best Sellers</h2>
                <div className="book-list-wrap">
                            {
                            books.map((book) => {
                                return <Book title={book.title} img={book.img} key={book.id} rating={book.rating} price={book.price}/>   
                            } )
                            }
                </div>
            </div>
        </section>
    )
};

const Book = (props) =>{
    const [{} , dispatch] = UseStateValue();

    const addToCart = () => {
        dispatch({
            type:'ADD_TO_BASKET',
            item:{
                id:props.id,
                title:props.title,
                image:props.img,
                price:props.price,
                rating:props.rating,
            },
        });
    };
    const viewItem = () => {
        dispatch({
            type:'VIEW_ITEM',
            item:{
                title:props.title,
                image:props.img,
                price:props.price,
                rating:props.rating,
            },
        });
    };
 return      <div className="book-item" >         
        <ThumbImage img={props.img}  />
        <div className="pdt_rating">
                {
                Array(props.rating)
                    .fill()
                    .map(() =>(
                        <p>⭐</p>
                    ))} 
                    <span>{props.rating}/5</span>
            </div>
         <Title title={props.title}/>
         <p className="pdt_price">$: <span>{props.price}</span> </p>
         <button className="btn  btn-yellow" onClick={addToCart}>Add to cart</button>
         <Link  to="/product-info" className="btn  btn-yellow" onClick={viewItem}>VIew</Link>


         </div>
};

const ThumbImage = ({img}) => <img src={img} alt="book" />
   
const Title = ({title}) => <p>{title}</p>;



export default BookList;
