import React from 'react'



import Product from './components/Product';
const Products = () => {
    return (
        <>
            <div className="container">

            <div className="products__container ">
                <div className="row">
                    {/* <div className="col-md-3">
                        <Product pdtImg="https://m.media-amazon.com/images/I/81NfRt2RF3L._UX695_.jpg" pdtName="Sparx Men's Running Shoes" pdtRating={4} pdtPrice={950.00} buyLink="https://amzn.to/3BPCoUL"/>
                    </div> */}
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71r69Y7BSeL._SX679_.jpg" pdtName="Samsung Galaxy M12 (Blue,4GB RAM, 64GB Storage) 6000 mAh with 8nm Processor | True 48 MP Quad Camera | 90Hz Refresh Rate" pdtRating={4} pdtPrice={9499.00} buyLink="https://www.amazon.in/Samsung-Galaxy-M12-Storage-Processor/dp/B08XGDN3TZ?keywords=Samsung+Galaxy+M12+%28Blue%2C4GB+RAM%2C+64GB+Storage%29+6000+mAh+with+8nm+Processor%7CTrue+48+MP+Quad+Camera%7C90Hz+Refresh+Rate&qid=1636173059&qsid=262-9008107-4518621&sr=8-2&sres=B08XGDN3TZ%2CB094YCY3L9%2CB08XJG8MQM%2CB08XJBPCTR%2CB08ZCXZ7F1%2CB09CV13RWF%2CB09576CYNP%2CB097RDVDL2%2CB098NGDNMT%2CB09CTYGJSD%2CB097RD2JX8%2CB096VD6RQG%2CB089MS8WGB%2CB008P7IM3C%2CB01C7L5XVK%2CB09J2V1G98&srpt=CELLULAR_PHONE&linkCode=ll1&tag=theajstore0e-21&linkId=8f16c0876e686cab3e08d18ea7b8750d&language=en_IN&ref_=as_li_ss_tl" key={1}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71A9Vo1BatL._SX679_.jpg" pdtName="Redmi 9 (Sky Blue, 4GB RAM, 64GB Storage) | 2.3GHz Mediatek Helio G35 Octa core Processor" pdtRating={4} pdtPrice={8499.00} buyLink="https://amzn.to/3bJUCfR" key={2}/>
                    </div>
                
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71os5DRhuSL._SY741_.jpg" pdtName="Samsung Galaxy M32 5G (Sky Blue, 6GB RAM, 128GB Storage)" pdtRating={4} pdtPrice={16999.00} buyLink="https://amzn.to/3qaziZe" key={3}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/81sZamLSPWL._SX679_.jpg" pdtName="Redmi Note 10S (Cosmic Purple, 6GB RAM, 128GB Storage) - Super Amoled Display | 64 MP Quad Camera | Alexa Built in
" pdtRating={4} pdtPrice={15499.00} buyLink="https://amzn.to/3o9hECK" key={4}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/81PQCRubagS._SX679_.jpg" pdtName="Mi 11X 5G (Celestial Silver 6GB RAM 128GB ROM | SD 870 | DisplayMate A+ rated E4 AMOLED | Upto 18 Months No Cost EMI)" pdtRating={4} pdtPrice={27999.00} buyLink="https://amzn.to/3H2UyWG" key={5}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/61LSu-Gto3L._SY741_.jpg" pdtName="Vivo Y33s (Middday Dream, 8GB RAM, 128GB Stoarge) with No Cost EMI/Additional Exchange Offers" pdtRating={4} pdtPrice={18990.00} buyLink="https://amzn.to/3bNZ2Cj" key={6}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/61gRGQe8jjL._SY741_.jpg" pdtName="Vivo Y21 (Midnight Blue, 4GB RAM, 64GB Storage) with No Cost EMI/Additional Exchange Offers" pdtRating={4} pdtPrice={13990.00} buyLink="https://amzn.to/3mQCvLA" key={7}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71b5BwTCcZL._SX569_.jpg" pdtName="Samsung Galaxy M52 5G (Blazing Black, 6GB RAM, 128GB Storage) Latest Snapdragon 778G 5G | sAMOLED 120Hz Display" pdtRating={4} pdtPrice={29999.00} buyLink="https://amzn.to/3wmjsvK" key={8}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/61iy2Ru9KdS._SX679_.jpg" pdtName="OnePlus Nord CE 5G (Blue Void, 8GB RAM, 128GB Storage)" pdtRating={4} pdtPrice={24999.00 } buyLink="https://amzn.to/3qiFE90" key={9}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71E+AgYDKtL._SX679_.jpg" pdtName="OnePlus 9 Pro 5G (Stellar Black, 8GB RAM, 128GB Storage)" pdtRating={4} pdtPrice={60999.00} buyLink="https://amzn.to/3kfTWn1" key={10}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71XmlboxNZL._SL1500_.jpg" pdtName="Xiaomi 11 Lite NE 5G (Jazz Blue 6GB RAM 128 GB Storage) | Slimmest (6.81mm) & Lightest (158g) 5G Smartphone | 10-bit AMOLED with Dolby Vision | Additional Off up to 6000 on Exchange" pdtRating={4} pdtPrice={26999.00} buyLink="https://amzn.to/3o67uCK" key={11}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/61fy+u9uqPL._SX522_.jpg" pdtName="OnePlus 9 5G (Arctic Sky, 8GB RAM, 128GB Storage)" pdtRating={4} pdtPrice={46999.00} buyLink="https://amzn.to/2ZYRutO" key={12}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71w4n2itCNL._SY741_.jpg" pdtName="Mi 10i 5G (Atlantic Blue, 6GB RAM, 128GB Storage) - 108MP Quad Camera | Snapdragon 750G Processor" pdtRating={4} pdtPrice={21999.00} buyLink="https://amzn.to/3mTGAhV" key={13}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/715fp8nWEmL._SX679_.jpg" pdtName="Redmi Note 10 Lite Aurora Blue 4GB RAM 128GB ROM | Alexa Built-in" pdtRating={4} pdtPrice={15999.00} buyLink="https://amzn.to/3wrEYzl" key={14}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/61T9-LABi9S._SX522_.jpg" pdtName="ASUS ZenBook 13(2021) - Intel Core i5-1135G7 11th Gen 13” FHD OLED Thin and Light Laptop (16GB RAM/512GB NVMe SSD/Win. 10/MS Off. H&S/Intel Iris Xe Graphics/Pine Grey/1.17 kg/1 Yr.), UX325EA-KG512TS" pdtRating={4} pdtPrice={84880.00} buyLink="https://amzn.to/2YqVYsT" key={15}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/81zr9PW9YtS._SX522_.jpg" pdtName="HP Spectre x360 11th Gen Intel Core i5 13.5-inch(34.2 cm) WUXGA Convertible Laptop (16GB RAM/512GB SSD/3:2 Aspect Ratio/400 Nits/Windows 10/MS Office/Poseidon Blue/1.34 Kg), 14-ea0538TU" pdtRating={4} pdtPrice={125300.00} buyLink="https://amzn.to/3D2pkwQ" key={16}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/711M3aJuhZL._SX522_.jpg" pdtName="HP Envy x360 Ryzen 5 13.3-inch(33.8 cm) FHD Touchscreen Convertible Laptop (16GB/512GB SSD/Windows 11 Home/MS Office 2019/Nightfall Black/1.32kg), 13-ay0533AU" pdtRating={4} pdtPrice={84490.00} buyLink="https://amzn.to/3mTjV5g" key={17}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71hmqIQJFdL._SX679_.jpg" pdtName="HP Pavilion (2021) Intel 11th Gen Core i5 14 inches FHD Screen Thin & Light Laptop, 16GB RAM, 512GB SSD, Iris Xe Graphics, Windows 11, MS Office, Backlit Keyboard, 1.41kg, Natural Silver (14-dv0054TU)" pdtRating={4} pdtPrice={66990.00} buyLink="https://amzn.to/3bOxwof" key={18}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71vFKBpKakL._SX679_.jpg" pdtName="2020 Apple MacBook Air (13.3-inch/33.78 cm, Apple M1 chip with 8‑core CPU and 7‑core GPU, 8GB RAM, 256GB SSD) - Gold" pdtRating={4} pdtPrice={88490.00} buyLink="https://amzn.to/3bOe5Mv" key={19}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/51LbXCEndSL._SX679_.jpg" pdtName="Crucial BX500 240GB 3D NAND SATA 6.35 cm (2.5-inch) SSD (CT240BX500SSD1)" pdtRating={4} pdtPrice={2483.00} buyLink="https://amzn.to/3CXPP6k" key={20}/>
                    </div>
                    <div className="col-md-3">
                    <Product pdtImg="https://m.media-amazon.com/images/I/71SMr4ZGf1L._SX679_.jpg" pdtName="HP 15 Ryzen 3 Thin & Light 15.6-inch (39.6 cms) FHD Laptop (Ryzen 3 3250U/8GB/256GB SSD/Windows 10/MS Office/1.69 kg), 15s-gy0501AU, Silver" pdtRating={4} pdtPrice={38990.00} buyLink="https://amzn.to/3EXU0Q9" key={21}/>
                    </div>
                </div>

            </div>
            </div>
        </>
    )
}

export default Products;
