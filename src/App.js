import {BrowserRouter as Router, Switch , Route} from "react-router-dom";
import Homepage from "./Homepage";
import Login from "./Login";
import Products from "./Products";
import Checkout from "./Checkout";
import ProductInfo from "./ProductInfo";
import BookList from "./BookList";
import ContactMe from "./ContactMe";
import Weather from "./Weather";
import Header from "./components/header";
import LogOutHeader from "./components/LogOutHeader";
import {useState , useEffect} from 'react'
function App() {

  const isUserIn = (value)=>{
    setIsLoggedIn(value)
    localStorage.setItem('UserLoggedIn', `${value}`);
    console.log("inside isuserin");
}

const [isLoggedIn , setIsLoggedIn] = useState(false);


useEffect(() => {
        const userLoggedInfo = localStorage.getItem('UserLoggedIn');
        if(userLoggedInfo === '1' || userLoggedInfo === 'true' ){
            setIsLoggedIn(true)
    
        }
},[isLoggedIn])
  return (
    <Router>
    <div className="my-app ">
    {isLoggedIn ?  <Header logegedIn={isUserIn}/>: <LogOutHeader />}

     
      <Switch>
        <Route exact path="/">
           {isLoggedIn ? <Homepage/> : <Login logegedIn={isUserIn} />}
        </Route>
        <Route exact path="/products">
        {isLoggedIn ? <Products/> : <Login logegedIn={isUserIn} />}
        </Route>
        {/* <Route exact path="/login">
        {isLoggedIn ? <Login/> : <Login logegedIn={isUserIn} />}
        </Route> */}
        <Route exact path="/checkout">
        {isLoggedIn ? <Checkout/> : <Login logegedIn={isUserIn} />}
        </Route>
        <Route exact path="/product-info">
        {isLoggedIn ? <ProductInfo/> : <Login logegedIn={isUserIn} />}
        </Route>
        <Route exact path="/book-list">
        {isLoggedIn ? <BookList/> : <Login logegedIn={isUserIn} />}
        </Route>
        <Route exact path="/contact">
        {isLoggedIn ? <ContactMe/> : <Login logegedIn={isUserIn} />}
        </Route>
        <Route exact path="/weather">
        {isLoggedIn ? <Weather/> : <Login logegedIn={isUserIn} />}
        </Route>
      </Switch>   
    </div>
    </Router>
  );
}

export default App;
