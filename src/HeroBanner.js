import React from 'react'
import bgImg from './uploads/banner-bg.jpg'
import { Link } from 'react-router-dom'
const HeroBanner = () => {
  return (
    <section className="hero_banner_new">

      <div className="banner-bg-wrap">
        <div className="bg-image-wrap">
          <img src={bgImg} alt="online shopping" />
        </div>
        <div className="banner-caption">
          <div className="container">
            <div className="caption-wrap">

              <h1>A never before shopping experience right here<span>!</span></h1>
              <Link to="/products" className="banner-btn">Best Deals</Link>
            </div>
            </div>
        </div>
      </div>
    </section>
  )
}

export default HeroBanner;
