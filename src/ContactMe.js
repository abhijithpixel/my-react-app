
import React from 'react';
import emailjs from 'emailjs-com';
import { useState } from 'react';
const ContactMe = () => {
  const [isSentMsg , setSentMsg] = useState(false);

    function sendEmail(e){
      e.preventDefault();
      emailjs.sendForm('service_ycwzcpj','template_jkwgtfa', e.target , 'user_RCfupbJIozNE0Uo7R3iDi').then(res=>{
      // console.log(res);
    if(res.status === 200){
      // alert("Message sent! ");
     isSentMsg ? setSentMsg(false) : setSentMsg(true);
     document.body.classList.add("mail_sent_msg");
      // console.log(isSentMsg);
      document.getElementById("frm_submit_btn").disabled = true;

    }
      }).catch(err=> console.log(err));
      e.target.reset();
    }
    function closeMsg(){
      document.body.classList.remove("mail_sent_msg");

    }
      function checkValue(){
        let inp1 = document.getElementById("username").value.length;
        let inp2 = document.getElementById("email").value.length;
        let inp3 = document.getElementById("mobile_number").value.length;

        if(inp1 > 2 && inp2 > 8 && inp3 > 8 ){
          document.getElementById("frm_submit_btn").disabled = false;
        }

      }
  return (
    <>

    <section className="contact_me_form"  >
    <div className="contact_me_form_wrap">
      <form onSubmit={sendEmail}>
        <h4>Keep in touch</h4>
        <fieldset>
          <label htmlFor="username">Name <span>*</span></label>
          <input type="text" id="username" name="username" autoComplete="off" onKeyPress={()=> checkValue()}/>
        </fieldset>
        <fieldset>
          <label htmlFor="email">Email <span>*</span></label>
          <input type="text" id="email" name="email" autoComplete="off" onKeyPress={()=> checkValue()}/>
        </fieldset>
        <fieldset>
          <label htmlFor="mobile_number">Mobile Number <span>*</span></label>
          <input type="text" id="mobile_number" name="mobile_number" autoComplete="off" onKeyPress={()=> checkValue()}/>
        </fieldset>
        <fieldset>
          <label htmlFor="message">Message</label>
         <textarea name="message" id="message" cols="30" rows="5" autoComplete="off"></textarea>
        </fieldset>
        <input id="frm_submit_btn" type="submit" onClick={() => sendEmail()} value="send " className="form_submit_btn" disabled/>
      </form>
    </div>
    </section>
    <div className="form_mail_sent_success_msg">
        <div className="form_mail_sent_success_msg_wrap">
          <p>Message Sent!</p>
          <span onClick={()=> closeMsg()}>OK</span>
        </div>
    </div>
    </>
  )
}

export default ContactMe;
