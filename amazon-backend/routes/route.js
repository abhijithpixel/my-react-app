const express = require("express");
const router = express.Router();
const Weather = require("../models/model");

router.route("/create").post((req,res)=>{
  const place = req.body.place;
  const temp = req.body.temp;
  const newWeather = new Weather({
    place,
    temp
  });

  newWeather.save();
})

router.route("/weathers").get((req,res)=>{
  Weather.find()
  .then(foundWeather => res.json(foundWeather))
  console.log(foundWeather);
})

module.exports = router;