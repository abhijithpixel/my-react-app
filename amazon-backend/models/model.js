const mongoose = require("mongoose");
const weatherSchema = {
  place : String,
  temp : String
}

const Weather = mongoose.model("Weather", weatherSchema);
module.exports = Weather;